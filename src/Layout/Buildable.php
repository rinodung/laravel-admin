<?php

namespace Rinodung\Admin\Layout;

interface Buildable
{
    public function build();
}
