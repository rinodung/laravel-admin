<?php

namespace Rinodung\Admin\Form\Field;

class Year extends Date
{
    protected $format = 'YYYY';
}
