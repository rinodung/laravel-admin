<?php

namespace Rinodung\Admin\Form\Field;

use Rinodung\Admin\Form\Field;

class Divide extends Field
{
    public function render()
    {
        return '<hr>';
    }
}
